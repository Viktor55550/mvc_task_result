﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using User.Core.ConnectingApi;
using User.Core.Model;
using User.Core.CoreController;
using Microsoft.AspNetCore.Http;

namespace UserMVC.Controllers
{
    public class CustomerController : Controller
    {
        public IActionResult CustomerSearch()
        {
            /*SearchCustomer search = new SearchCustomer();
            search.IsAdmin = Convert.ToBoolean(HttpContext.Session.GetString("IsAdmin"));
*/
            return View();
        }

        public IActionResult MainCustomer()
        {
            return View();
        }

        public IActionResult AddCustomer()
        {
            return View();
        }

        public IActionResult AddUser()
        {
            return View();
        }

        public IActionResult DeleteCustomer()
        {
            return View();
        }


        [HttpPost]
        public ViewResult CustomerSearch(SearchCustomer customer)
        {
            SearchCustomer customerResult = new SearchCustomer();
            customerResult = CoreCustomerController.Search(customer);

            if (customerResult.responses.Count != 0)
                return View(customerResult);
            else
            {
                ViewBag.NoneCustomer = "Customer not found";
                return View();
            }
        }

        [HttpPost]
        public ViewResult GetAllInfo(SearchCustomer customer)
        {
            customer.Id = 6;
            SearchCustomer customerResult = new SearchCustomer();
            customerResult = CoreCustomerController.GetCutomerById(customer);

            return View("MainCustomer", customerResult);
        }

        [HttpPost]
        public ViewResult SaveCustomer(SearchCustomer customer)
        {

            if (ModelState.IsValid)
            {
                var x = CoreCustomerController.SaveCustomer(customer);
                var y = CoreCustomerController.SaveCustomerDetail(customer);

                if ((int)x.Result == 1 && (int)y.Result == 1)
                {
                    return GetAllInfo(customer);
                }
                else
                {
                    ViewBag.ErrorMessage = x.Description;
                    return View("AddUser");
                }
            }
            else
            {
                return View("AddCustomer");
            }
        }

        [HttpPost]
        public ViewResult DeleteCustomer(DeleteById customer)
        {

            if (ModelState.IsValid)
            {
                //var x = CoreCustomerController.DeleteCustomer(customer);
                var y = CoreCustomerController.DeleteCustomerDetail(customer);

                if ((int)y.Result == 1)//(int)x.Result == 1 && (int)y.Result == 1)
                {
                    ViewBag.Done = "Customer has deleted";
                    return View("DeleteCustomer");//GetAllInfo(customer);
                }
                else
                {
                    ViewBag.ErrorMessage = y.Description;
                    return View("AddUser");
                }
            }
            else
            {
                return View("DeleteCustomer");
            }
        }

    }
}
