﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using User.Core.CoreController;
using User.Core.Helper;
using User.Core.Model;
using UserMVC.Models;

namespace UserMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Login()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult LoginUser(Users login)
        {
            if (ModelState.IsValid)
            {
                SearchCustomer customer = new SearchCustomer();
                login.UserPassword = MD5.CreateMD5(login.UserPassword);
                var x = UserController.LoginUserAsync(login);

                if (x.CustomerId != 0)
                {
                    customer.IsAdmin = x.IsAdmin;
                    //HttpContext.Session.SetString("IsAdmin", x.IsAdmin.ToString());
                    return View("~/Views/Customer/CustomerSearch.cshtml", customer);
                }
                else
                {
                    ViewBag.ErrorMessage = "Wrong UserName or passowrd";
                    return View("Login");
                }
            }
            else
            {
                return View("Login");
            }
        }
    }
}
