﻿using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Configuration;
using User.Core.Helper;

namespace User.Core.ConnectingApi
{
    public class ConnectApi
    {
        public static string CallApiMethod(dynamic dynamic, string callMethod)
        {
            string result = string.Empty;

            try
            {
                HttpClient client = new HttpClient();
                var xppp = ConfigurationManager.AppSettings["ApiBaseAddress"];
                client.BaseAddress = new Uri("https://localhost:5001");//
                //var xp = ConfigurationManager.ConnectionStrings["ApiBaseAddress"].ConnectionString;
                client.DefaultRequestHeaders.Accept.Add(
                   new MediaTypeWithQualityHeaderValue("application/json"));
                string requestUrl = "api/" + callMethod;
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(dynamic);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                var x = client.PostAsync(requestUrl, stringContent).Result;

                var y = x.Content.ReadAsStringAsync();

                result = y.Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                result = "{\"result\":0,\"description\":\"Cannot connect API\"}";
            }

            return result;
        }
    }
}
