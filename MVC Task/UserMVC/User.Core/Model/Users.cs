﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace User.Core.Model
{
    public class Users
    {
        [Required(ErrorMessage = "Please enter UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        public string UserPassword { get; set; }

        public int CustomerId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
