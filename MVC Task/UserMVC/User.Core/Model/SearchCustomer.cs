﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace User.Core.Model
{
    public class SearchCustomer : Users
    {
        public List<SearchCustomer> responses { get; set; }
        public SearchCustomer searchAtribute { get; set; }
        public IEnumerable<SearchCustomer> Customers { get { return responses; } }

        public void AddGuest(SearchCustomer customer)
        {
            responses.Add(customer);
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter Last Name")]
        public string CustomerName { get; set; }
        
        [Required(ErrorMessage = "Please enter Surname")]
        public string CustomerSurname { get; set; }

        [Required(ErrorMessage = "Please enter Father Name")]
        public string FathersName { get; set; }

        [Required(ErrorMessage = "Please enter Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Passport")]
        public string PersonDocument { get; set; }

        [Required(ErrorMessage = "Please enter Date of birth")]
        public DateTime? DateOfBirth { get; set; }

        [Required(ErrorMessage = "Please enter Gender")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please enter Address")]
        public string CustomerAddress { get; set; }

        [Required(ErrorMessage = "Please enter City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Please enter region")]
        public string Region { get; set; }

        [Required(ErrorMessage = "Please enter postal code")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Please enter phone")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please enter Fax")]
        public string Fax { get; set; }


    }
}
