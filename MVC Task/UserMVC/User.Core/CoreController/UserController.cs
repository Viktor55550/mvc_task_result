﻿using Nancy.Json;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using User.Core.Helper;
using ActionResult = User.Core.Helper.ActionResult;
using System.Threading.Tasks;
using User.Core.Model;
using User.Core.ConnectingApi;

namespace User.Core.CoreController
{
    public class UserController
    {

        public static Users LoginUserAsync(Users login)
        {
            Users result = new Users();

            string loginResultString = ConnectApi.CallApiMethod(login, "Customer/LoginCustomerDetail");

            JavaScriptSerializer js = new JavaScriptSerializer();

            dynamic blogObject = js.Deserialize<dynamic>(loginResultString);

            result.CustomerId = Convert.ToInt32(blogObject["CustomerId"]);
            result.IsAdmin = Convert.ToBoolean(blogObject["IsAdmin"]);

            return result;
        }

    }
}
