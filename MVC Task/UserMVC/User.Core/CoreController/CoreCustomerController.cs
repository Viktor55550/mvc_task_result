﻿using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Text;
using User.Core.ConnectingApi;
using User.Core.Helper;
using User.Core.Model;

namespace User.Core.CoreController
{
    public class CoreCustomerController
    {
        public static SearchCustomer Search(SearchCustomer search)
        {
            SearchCustomer result = new SearchCustomer();
            result.responses = new List<SearchCustomer>();
            string searchResultSting = ConnectApi.CallApiMethod(search, "User/SearchCustomer");

            JavaScriptSerializer js = new JavaScriptSerializer();
            List<dynamic> blogObject = js.Deserialize<List<dynamic>>(searchResultSting);

            foreach (var item in blogObject)
            {
                SearchCustomer customer = new SearchCustomer();

                customer.Id = Convert.ToInt32(item["Id"]);
                customer.CustomerName = item["CustomerName"];
                customer.CustomerSurname = item["CustomerSurname"];
                customer.FathersName = item["FathersName"];
                customer.Email = item["Email"];
                customer.PersonDocument = item["PersonDocument"];
                customer.City = item["City"];

                result.AddGuest(customer);
            }

            return result;
        }

        public static SearchCustomer GetCutomerById(SearchCustomer customer)
        {
            SearchCustomer result = new SearchCustomer();
            result.responses = new List<SearchCustomer>();
            dynamic objSend = new { id = customer.Id };
            string searchResultSting = ConnectApi.CallApiMethod(objSend, "Customer/GetCustomers");

            JavaScriptSerializer js = new JavaScriptSerializer();
            dynamic blogObject = js.Deserialize<dynamic>(searchResultSting);

            result.Id = Convert.ToInt32(blogObject["Id"]);
            result.CustomerName = blogObject["CustomerName"];
            result.CustomerSurname = blogObject["CustomerSurname"];
            result.FathersName = blogObject["FathersName"];
            result.Email = blogObject["Email"];
            result.PersonDocument = blogObject["PersonDocument"];
            result.City = blogObject["City"];


            return result;
        }


        public static ActionResult SaveCustomer(SearchCustomer customer)
        {
            ActionResult result = new ActionResult();

            string loginResultString = ConnectApi.CallApiMethod(customer, "Customer/SaveCustomers");

            JavaScriptSerializer js = new JavaScriptSerializer();

            dynamic blogObject = js.Deserialize<dynamic>(loginResultString);

            result.Result = (ResultCode)Convert.ToInt32(blogObject["result"]);
            result.Description = blogObject["Description"];

            return result;
        }

        public static ActionResult SaveCustomerDetail(SearchCustomer customer)
        {
            ActionResult result = new ActionResult();

            string loginResultString = ConnectApi.CallApiMethod(customer, "Customer/SaveCustomerDetails");

            JavaScriptSerializer js = new JavaScriptSerializer();

            dynamic blogObject = js.Deserialize<dynamic>(loginResultString);

            result.Result = (ResultCode)Convert.ToInt32(blogObject["result"]);
            result.Description = blogObject["Description"];

            return result;
        }

        public static ActionResult DeleteCustomer(SearchCustomer customer)
        {
            ActionResult result = new ActionResult();

            string loginResultString = ConnectApi.CallApiMethod(customer, "Customer/DeleteCustomers");

            JavaScriptSerializer js = new JavaScriptSerializer();

            dynamic blogObject = js.Deserialize<dynamic>(loginResultString);

            result.Result = (ResultCode)Convert.ToInt32(blogObject["result"]);
            result.Description = blogObject["Description"];

            return result;
        }

        public static ActionResult DeleteCustomerDetail(DeleteById customer)
        {
            ActionResult result = new ActionResult();

            string loginResultString = ConnectApi.CallApiMethod(customer, "Customer/DeleteCustomerDetails");

            JavaScriptSerializer js = new JavaScriptSerializer();

            dynamic blogObject = js.Deserialize<dynamic>(loginResultString);

            result.Result = (ResultCode)Convert.ToInt32(blogObject["result"]);
            result.Description = blogObject["Description"];

            return result;
        }
    }
}

