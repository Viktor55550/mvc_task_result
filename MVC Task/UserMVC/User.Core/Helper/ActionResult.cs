﻿using System;
using System.Collections.Generic;
using System.Text;

namespace User.Core.Helper
{
    public class ActionResult
    {
        public ResultCode Result { get; set; }

        public string Description { get; set; }

        public ActionResult()
        {

        }

        public ActionResult(ResultCode resultCode, string description)
        {
            Result = resultCode;
            Description = description;
        }
    }

    public enum ResultCode
    {
        Faild,
        Done
    }
}
