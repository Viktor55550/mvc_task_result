﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.DAL.Helpers;
using User.DAL.Models;

namespace User.API.Validation
{
    public class CustomerValidation
    {
        public static ActionResult ValidationDeleteCustomer(Customer customer)
        {
            ActionResult result = new ActionResult();

            if (customer.Id == 0)
            {
                result.Description = "Id can't be 0";
                result.Result = ResultCode.Faild;
            }

            return result;
        }
    }
}
