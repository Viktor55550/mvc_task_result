﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.DAL.Helpers;
using User.DAL.Models;
using ActionResult = User.DAL.Helpers.ActionResult;

namespace UsersMVC.DAL.Validation
{
    public class CustomerDetailValidation
    {
        public static ActionResult ValidationDeleteCustomerDetail(CustomerDetail customerDetail)
        {
            ActionResult result = new ActionResult();

            if (customerDetail.CustomerId == 0)
            {
                result.Description = "CustomerId can't be 0";
                result.Result = ResultCode.Faild;
            }

            return result;
        }
    }
}
