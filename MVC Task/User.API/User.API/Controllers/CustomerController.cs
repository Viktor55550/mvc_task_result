﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using User.API.Validation;
using User.DAL.Helpers;
using User.DAL.Models;
using User.DAL.Operations;
using UsersMVC.DAL.Validation;
using ActionResult = User.DAL.Helpers.ActionResult;

namespace UsersMVC.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private static IConfiguration _configuration;

        public CustomerController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public static IConfigurationRoot GetConnection()
        {

            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appSettings.json").Build();

            return builder;
        }

        public string connDB = GetConnection().GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;

        [HttpPost("SaveCustomers")]
        public ActionResult SaveCustomers(Customer customer)
        {
            return CustomerOperation.SaveCustomer(customer, connDB);
        }

        [HttpGet("GetCustomers")]
        public Customer GetCustomers(Customer customer)
        {
            //string connStr = ConfigurationManager.ConnectionStrings["myString"].ConnectionString;
            //string connString = this.Configuration.GetConnectionString("MyConn");
            //string myConnectionString = _configuration.GetConnectionString("DefaultConnection");

            return CustomerOperation.GetCustomer(customer, connDB);
        }

        [HttpPut("UpdateCustomers")]
        public ActionResult UpdateCustomers(Customer customer)
        {
            return CustomerOperation.UpdateCustomer(customer, connDB);
        }

        [HttpDelete("DeleteCustomers")]
        public ActionResult DeleteCustomers(Customer customer)
        {
            ActionResult result = new ActionResult();
            result = CustomerValidation.ValidationDeleteCustomer(customer);

            if (result.Result == ResultCode.Faild)
                return result;

            return CustomerOperation.DeleteCustomer(customer, connDB);
        }


        [HttpPost("SaveCustomerDetails")]
        public ActionResult SaveCustomerDetails(CustomerDetail customerDetail)
        {
            return CustomerDetailOperation.SaveCustomerDetail(customerDetail, connDB);
        }

        [HttpGet("GetCustomerDetails")]
        public List<CustomerDetail> GetCustomerDetails(CustomerDetail customerDetail)
        {
            return CustomerDetailOperation.GetCustomerDetail(customerDetail, connDB);
        }

        [HttpPost("LoginCustomerDetail")]
        public CustomerDetail LoginCustomerDetail(CustomerDetail customerDetail)
        {
            return CustomerDetailOperation.LoginCustomerDetail(customerDetail, connDB);
        }
        


        [HttpPut("UpdateCustomerDetails")]
        public ActionResult UpdateCustomerDetails(CustomerDetail customerDetail)
        {
            return CustomerDetailOperation.UpdateCustomerDetail(customerDetail, connDB);
        }

        [HttpPost("DeleteCustomerDetails")]
        public ActionResult DeleteCustomerDetails(CustomerDetail customerDetail)
        {
            ActionResult result = new ActionResult();
            result = CustomerDetailValidation.ValidationDeleteCustomerDetail(customerDetail);

            //if (result.Result == ResultCode.Faild)
                //return result;
            
            return CustomerDetailOperation.DeleteCustomerDetail(customerDetail, connDB);
        }

    }
}
