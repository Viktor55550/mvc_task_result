﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using User.DAL.Models;
using User.DAL.Operations;
using ActionResult = User.DAL.Helpers.ActionResult;

namespace User.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public static IConfigurationRoot GetConnection()
        {

            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appSettings.json").Build();

            return builder;
        }

        public string connDB = GetConnection().GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;

        [HttpPost("Login")]
        public ActionResult Login(CustomerDetail user)
        {
            return UserOperation.Login(user, connDB);
        }

        [HttpPost("SearchCustomer")]
        public List<Customer> SearchCustomer(Customer user)
        {
            return UserOperation.Search(user, connDB);
        }
    }
}
