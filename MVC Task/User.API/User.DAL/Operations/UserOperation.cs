﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using User.DAL.Helpers;
using User.DAL.Models;
using ActionResult = User.DAL.Helpers.ActionResult;

namespace User.DAL.Operations
{
    public class UserOperation
    {
        public static ActionResult Login(CustomerDetail user, string connString)
        {
            ActionResult result = new ActionResult();

            using (SqlConnection conn = new SqlConnection(connString))//"DefaultConnection"))//ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString())
            {
                string InsertQuery = @"SELECT TOP 1 1 FROM CustomerDetails WHERE UserName = @userName and UserPassword = @password";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();

                    cmd.Parameters.Add("@userName", System.Data.SqlDbType.NVarChar, 50).Value = user.UserName;
                    cmd.Parameters.Add("@password", System.Data.SqlDbType.NVarChar, 50).Value = user.UserPassword;

                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        if (dr.Read())
                        {
                            result.Result = ResultCode.Done;
                            result.Description = "Welcome";
                        }
                        else
                        {
                            result.Result = ResultCode.Faild;
                            result.Description = "Invalid UserName or Password";
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }

            return result;

        }

        public static List<Customer> Search(Customer customer, string connString)
        {
            List<Customer> result = new List<Customer>();
            DataTable dt = new DataTable();


            using (SqlConnection conn = new SqlConnection(connString))//"DefaultConnection"))//ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString())
            {

                SqlCommand cmd = new SqlCommand("SearchCustomers", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();

                if (!string.IsNullOrEmpty(customer.CustomerName))
                {
                    cmd.Parameters.Add("@customerName", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerName;
                }

                if (!string.IsNullOrEmpty(customer.CustomerSurname))
                {
                    cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                }

                if (!string.IsNullOrEmpty(customer.FathersName))
                {
                    cmd.Parameters.Add("@fathersName", System.Data.SqlDbType.NVarChar, 50).Value = customer.FathersName;
                }

                if (!string.IsNullOrEmpty(customer.Email))
                {
                    cmd.Parameters.Add("@email", System.Data.SqlDbType.NVarChar, 50).Value = customer.Email;
                }



                try
                {
                    SqlDataReader dr = cmd.ExecuteReader();
                    dt.Load(dr);
                    foreach (DataRow item in dt.Rows)
                    {
                        Customer person1 = new Customer();

                        person1.Id = Convert.ToInt32(item["Id"].ToString());
                        person1.CustomerName = item["CustomerName"].ToString();
                        person1.CustomerSurname = item["CustomerSurname"].ToString();
                        person1.FathersName = item["FathersName"].ToString();
                        person1.Email = item["Email"].ToString();
                        person1.PersonDocument = item["PersonDocument"].ToString();

                        result.Add(person1);
                    }

                
                }
                catch (Exception ex)
            {
                throw;
            }

        }

            return result;

        }
}
}
