﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using User.DAL.Helpers;
using User.DAL.Models;

namespace User.DAL.Operations
{
    public class CustomerOperation
    {
        public static Customer GetCustomer(Customer customer,string connString)
        {
            Customer result = new Customer();
            DataTable dt = new DataTable();
            /*SqlConnectionStringBuilder con = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["MyString"].ConnectionString);
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["MyString"];
            string conStr = ConfigurationManager.ConnectionStrings["MyString"].ConnectionString;//.ToString();
            */
            using (SqlConnection conn = new SqlConnection(connString))//@"Data Source=LAPTOP-ST5MDDKK\SQLEXPRESS;Initial Catalog=UsersDB;Integrated Security=True"))//ConfigurationManager.ConnectionStrings["MyString"].ConnectionString))
            {
                string InsertQuery = @"SELECT * FROM Customers WHERE Id = CASE WHEN @id = 0 THEN Id ELSE @id END";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();

                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = customer.Id;

                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        dt.Load(dr);

                        foreach (DataRow item in dt.Rows)
                        {
                            result.Id = Convert.ToInt32(item["Id"].ToString());
                            result.CustomerName = item["CustomerName"].ToString();
                            result.CustomerSurname = item["CustomerSurname"].ToString();
                            result.FathersName = item["FathersName"].ToString();
                            result.Email = item["Email"].ToString();
                            result.PersonDocument = item["PersonDocument"].ToString();
                            result.DateOfBirth = Convert.ToDateTime(item["DateOfBirth"]);
                            result.Gender = item["Gender"] == DBNull.Value ? "" : item["Gender"].ToString();
                            result.CustomerAddress = item["CustomerAddress"].ToString();
                            result.City = item["City"].ToString();
                            result.Region = item["Region"].ToString();
                            result.PostalCode = item["PostalCode"].ToString();
                            result.Phone = item["Phone"].ToString();
                            result.Fax = item["Fax"].ToString();

                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        public static ActionResult SaveCustomer(Customer customer, string connString)
        {
            ActionResult result = new ActionResult();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(connString))
            {
                string InsertQuery = @"INSERT INTO Customers(CustomerName,CustomerSurname,FathersName,Email,PersonDocument,DateOfBirth,Gender,CustomerAddress,City,Region,PostalCode,Phone,Fax)
                                       VALUES (@customerName,@customerSurname,@fathersName,@email,@personDocument,@dateOfBirth,@gender,@customerAddress,@city,@region,@postalCode,@phone,@fax)";


                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.Add("@customerName", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerName;
                    cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    cmd.Parameters.Add("@fathersName", System.Data.SqlDbType.NVarChar, 50).Value = customer.FathersName;
                    cmd.Parameters.Add("@email", System.Data.SqlDbType.NVarChar, 50).Value = customer.Email;
                    cmd.Parameters.Add("@personDocument", System.Data.SqlDbType.NVarChar, 50).Value = customer.PersonDocument;
                    cmd.Parameters.Add("@dateOfBirth", System.Data.SqlDbType.DateTime, 50).Value = customer.DateOfBirth;
                    cmd.Parameters.Add("@gender", System.Data.SqlDbType.NVarChar, 50).Value = customer.Gender;
                    cmd.Parameters.Add("@customerAddress", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerAddress;
                    cmd.Parameters.Add("@city", System.Data.SqlDbType.NVarChar, 50).Value = customer.City;
                    cmd.Parameters.Add("@region", System.Data.SqlDbType.NVarChar, 50).Value = customer.Region;
                    cmd.Parameters.Add("@postalCode", System.Data.SqlDbType.NVarChar, 50).Value = customer.PostalCode;
                    cmd.Parameters.Add("@phone", System.Data.SqlDbType.NVarChar, 50).Value = customer.Phone;
                    cmd.Parameters.Add("@fax", System.Data.SqlDbType.NVarChar, 50).Value = customer.Fax;

                    try
                    {
                        cmd.ExecuteNonQuery();
                        result.Result = ResultCode.Done;
                    }
                    catch (Exception ex)
                    {
                        result.Result = ResultCode.Faild;
                        result.Description = ex.StackTrace;
                    }
                }
            }

            return result;
        }

        
        public static ActionResult UpdateCustomer(Customer customer, string connString)
        {
            ActionResult result = new ActionResult();

            //make sure that connection string is valid senc ashxatuma sax
            using (SqlConnection conn = new SqlConnection(connString))
            {
                string InsertQuery = @"UPDATE Customers SET ";

                using (SqlCommand cmd = new SqlCommand())
                {
                    if (!string.IsNullOrEmpty(customer.CustomerName))
                    {
                        InsertQuery += "CustomerName = @customerName";
                        cmd.Parameters.Add("@customerName", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerName;
                    }

                    if (!string.IsNullOrEmpty(customer.CustomerSurname))
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }

                    if (!string.IsNullOrEmpty(customer.FathersName))
                    {
                        InsertQuery += customer.FathersName == null ? "" : ", " +
                            "FathersName = @fathersName";
                        cmd.Parameters.Add("@fathersName", System.Data.SqlDbType.NVarChar, 50).Value = customer.FathersName;
                    }
                    /*
                    if (!string.IsNullOrEmpty(customer.Email))
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "Email = @email";
                        cmd.Parameters.Add("@email", System.Data.SqlDbType.NVarChar, 50).Value = customer.Email;
                    }

                    if (!string.IsNullOrEmpty(customer.PersonDocument))
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }

                    if (customer.DateOfBirth != null)
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }

                    if (customer.Gender != null)
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }

                    if (!string.IsNullOrEmpty(customer.CustomerAddress))
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }

                    if (!string.IsNullOrEmpty(customer.City))
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }

                    if (!string.IsNullOrEmpty(customer.Region))
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }

                    if (!string.IsNullOrEmpty(customer.PostalCode))
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }

                    if (!string.IsNullOrEmpty(customer.Phone))
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }

                    if (!string.IsNullOrEmpty(customer.Fax))
                    {
                        InsertQuery += customer.CustomerSurname == null ? "" : ", " +
                            "CustomerSurname = @customerSurname";
                        cmd.Parameters.Add("@customerSurname", System.Data.SqlDbType.NVarChar, 50).Value = customer.CustomerSurname;
                    }*/

                    cmd.CommandText = InsertQuery + " where Id = @id";
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = customer.Id;


                    try
                    {
                        cmd.ExecuteNonQuery();
                        result.Result = ResultCode.Done;
                    }
                    catch (Exception ex)
                    {
                        result.Result = ResultCode.Faild;
                        result.Description = ex.StackTrace;
                    }
                }
            }

            return result;
        }

        public static ActionResult DeleteCustomer(Customer customer, string connString)
        {
            ActionResult result = new ActionResult();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(connString))
            {
                string InsertQuery = @"DELETE FROM Customers WHERE Id = CASE WHEN @id = 0 THEN Id ELSE @id END";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = customer.Id;

                    try
                    {
                        cmd.ExecuteNonQuery();
                        result.Result = ResultCode.Done;
                    }
                    catch (Exception ex)
                    {
                        result.Result = ResultCode.Faild;
                        result.Description = ex.StackTrace;
                    }
                }
            }

            return result;
        }

    }
}
