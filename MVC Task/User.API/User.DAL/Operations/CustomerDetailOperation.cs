﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using User.DAL.Helpers;
using User.DAL.Models;

namespace User.DAL.Operations
{
    public class CustomerDetailOperation
    {
        public static List<CustomerDetail> GetCustomerDetail(CustomerDetail customerDetail, string connString)
        {
            List<CustomerDetail> result = new List<CustomerDetail>();
            DataTable dt = new DataTable();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(connString))//"DefaultConnection"))//ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString())
            {
                string InsertQuery = @"SELECT * FROM CustomerDetails WHERE Id = CASE WHEN @id = 0 THEN Id ELSE @id END";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = customerDetail.Id;

                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        dt.Load(dr);

                        foreach (DataRow item in dt.Rows)
                        {
                            CustomerDetail personDetail1 = new CustomerDetail();

                            //person1.Id = item["Id"].ToString();
                            personDetail1.CustomerId = Convert.ToInt32(item["CustomerId"]);
                            personDetail1.UserPassword = item["UserPassword"].ToString();
                            personDetail1.RegisterDate = Convert.ToDateTime(item["RegisterDate"]);
                            personDetail1.IsAdmin = Convert.ToBoolean(item["IsAdmin"]);

                            result.Add(personDetail1);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        public static ActionResult SaveCustomerDetail(CustomerDetail customerDetail, string connString)
        {
            ActionResult result = new ActionResult();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(connString))
            {
                string InsertQuery = @"INSERT INTO CustomerDetails(CustomerId,UserPassword,RegisterDate,UserName,IsAdmin)
                                       VALUES ((select top 1 Id
from Customers		
order by id desc),@userPassword,@registerDate,@userName,@isAdmin)";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();

                    cmd.Parameters.Add("@userPassword", System.Data.SqlDbType.NVarChar).Value = MD5.CreateMD5(customerDetail.UserPassword);
                    cmd.Parameters.Add("@registerDate", System.Data.SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("@userName", System.Data.SqlDbType.NVarChar).Value = customerDetail.UserName;
                    cmd.Parameters.Add("@isAdmin", System.Data.SqlDbType.Bit).Value = customerDetail.IsAdmin;


                    try
                    {
                        cmd.ExecuteNonQuery();
                        result.Result = ResultCode.Done;
                    }
                    catch (Exception ex)
                    {
                        result.Result = ResultCode.Faild;
                        result.Description = ex.StackTrace;
                    }
                }
            }

            return result;
        }


        public static ActionResult UpdateCustomerDetail(CustomerDetail customerDetail, string connString)
        {
            ActionResult result = new ActionResult();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(connString))
            {
                string InsertQuery = @"UPDATE CustomerDetails SET ";

                using (SqlCommand cmd = new SqlCommand())
                {
                    /*if (customerDetail.CustomerId != 0)
                    {
                        InsertQuery += customerDetail.CustomerId == null ? "" : ", " +
                            "CustomerId = @customerId";
                        cmd.Parameters.Add("@customerId", System.Data.SqlDbType.NVarChar, 50).Value = customerDetail.CustomerId;
                    }*/

                    if (!string.IsNullOrEmpty(customerDetail.UserPassword))
                    {
                        InsertQuery += customerDetail.UserPassword == null ? "" : " " +
                            "UserPassword = @userPassword";
                        cmd.Parameters.Add("@userPassword", System.Data.SqlDbType.NVarChar, 50).Value = customerDetail.UserPassword;
                    }


                    /*if (customerDetail.RegisterDate != null)
                    {
                        InsertQuery += customerDetail == null ? "" : ", " +
                            "UserPassword = @userPassword";
                        cmd.Parameters.Add("@userPassword", System.Data.SqlDbType.NVarChar, 50).Value = customerDetail.UserPassword;
                    }*/

                    cmd.CommandText = InsertQuery + " where CustomerId = @customerId";
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@customerId", System.Data.SqlDbType.Int).Value = customerDetail.CustomerId;


                    try
                    {
                        cmd.ExecuteNonQuery();
                        result.Result = ResultCode.Done;
                    }
                    catch (Exception ex)
                    {
                        result.Result = ResultCode.Faild;
                        result.Description = ex.StackTrace;
                    }
                }
            }

            return result;
        }

        public static ActionResult DeleteCustomerDetail(CustomerDetail customerDetail, string connString)
        {
            ActionResult result = new ActionResult();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(connString))
            {
                string InsertQuery = @"DELETE FROM CustomerDetails WHERE CustomerId = CASE WHEN @id = 0 THEN Id ELSE @id END";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = customerDetail.CustomerId;

                    try
                    {
                        cmd.ExecuteNonQuery();
                        result.Result = ResultCode.Done;
                    }
                    catch (Exception ex)
                    {
                        result.Result = ResultCode.Faild;
                        result.Description = ex.StackTrace;
                    }
                }
            }

            return result;
        }

        public static CustomerDetail LoginCustomerDetail(CustomerDetail customerDetail, string connString)
        {
            CustomerDetail personDetail = new CustomerDetail();
            DataTable dt = new DataTable();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(connString))//"DefaultConnection"))//ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString())
            {
                string InsertQuery = @"SELECT * FROM CustomerDetails WHERE UserName = @userName and UserPassword = @password";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();

                    cmd.Parameters.Add("@userName", System.Data.SqlDbType.NVarChar, 50).Value = customerDetail.UserName;
                    cmd.Parameters.Add("@password", System.Data.SqlDbType.NVarChar, 50).Value = customerDetail.UserPassword;
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        dt.Load(dr);

                        foreach (DataRow item in dt.Rows)
                        {
                            //person1.Id = item["Id"].ToString();
                            personDetail.CustomerId = Convert.ToInt32(item["CustomerId"]);
                            personDetail.RegisterDate = Convert.ToDateTime(item["RegisterDate"]);
                            personDetail.IsAdmin = Convert.ToBoolean(item["IsAdmin"]);

                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }

            return personDetail;
        }
    }
}
