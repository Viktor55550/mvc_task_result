﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.DAL.Infrastructure.ModelAbstraction;

namespace User.DAL.Models
{
    public class Customer : ModelBaseWithId
    {
        public string CustomerName { get; set; }

        public string CustomerSurname { get; set; }

        public string FathersName { get; set; }

        public string Email { get; set; }

        public string PersonDocument { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Gender { get; set; }

        public string CustomerAddress { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string PostalCode { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }
    }
}
