﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.DAL.Infrastructure.ModelAbstraction;

namespace User.DAL.Models
{
    public class CustomerDetail : ModelBaseWithId
    {
        public int CustomerId { get; set; }

        public string UserName { get; set; }

        public string UserPassword { get; set; }

        public DateTime? RegisterDate { get; set; }

        public bool IsAdmin { get; set; }
    }
}
